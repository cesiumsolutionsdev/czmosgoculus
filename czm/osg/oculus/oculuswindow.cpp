#include "oculuswindow.h"
#include "oculusupdateslavecallback.h"
#include "oculuseventhandler.h"
#include "oculustouchmanipulator.h"
#include <osgDB/ReadFile>

namespace osgOculus
{
	void OculusScene::traverse(osg::NodeVisitor& nv)
	{
		// Must be realized before any traversal
		if (m_realizeOperation->realized())
		{
			if (!m_configured)
			{
				configure();
			}
		}

		osg::Group::traverse(nv);
	}

	/* Protected functions */
	void OculusScene::configure()
	{
		osg::ref_ptr<osg::GraphicsContext> gc = m_viewer->getCamera()->getGraphicsContext();

		// Attach a callback to detect swap
		osg::ref_ptr<OculusSwapCallback> swapCallback = new OculusSwapCallback(m_device);
		gc->setSwapCallback(swapCallback.get());

		osg::ref_ptr<osg::Camera> camera = m_viewer->getCamera();
		camera->setName("Main");
		osg::Vec4 clearColor = camera->getClearColor();

		// Create RTT cameras and attach textures
		osg::Camera* cameraRTTLeft = m_device->createRTTCamera(OculusDevice::Eye::LEFT, osg::Camera::ABSOLUTE_RF, clearColor, gc.get());
		osg::Camera* cameraRTTRight = m_device->createRTTCamera(OculusDevice::Eye::RIGHT, osg::Camera::ABSOLUTE_RF, clearColor, gc.get());
		cameraRTTLeft->setName("LeftRTT");
		cameraRTTRight->setName("RightRTT");

		// Add RTT cameras as slaves, specifying offsets for the projection
		m_viewer->addSlave(cameraRTTLeft,
			m_device->projectionMatrix(OculusDevice::Eye::LEFT),
			m_device->viewMatrix(OculusDevice::Eye::LEFT),
			true);
		m_viewer->getSlave(0)._updateSlaveCallback = new OculusUpdateSlaveCallback(OculusUpdateSlaveCallback::LEFT_CAMERA, m_device.get(), swapCallback.get());

		m_viewer->addSlave(cameraRTTRight,
			m_device->projectionMatrix(OculusDevice::Eye::RIGHT),
			m_device->viewMatrix(OculusDevice::Eye::RIGHT),
			true);
		m_viewer->getSlave(1)._updateSlaveCallback = new OculusUpdateSlaveCallback(OculusUpdateSlaveCallback::RIGHT_CAMERA, m_device.get(), swapCallback.get());

		// Use sky light instead of headlight to avoid light changes when head movements
		m_viewer->setLightingMode(osg::View::SKY_LIGHT);

		// this flag needs to be set to avoid the following GL warning at every frame:
		// Warning: detected OpenGL error 'invalid operation' at after RenderBin::draw(..)
		m_viewer->setReleaseContextAtEndOfFrameHint(false);

		// Disable rendering of main camera since its being overwritten by the swap texture anyway
		camera->setGraphicsContext(nullptr);

// 		osg::ref_ptr<osgViewer::DepthPartitionSettings> dps = new osgViewer::DepthPartitionSettings;
// 		dps->_mode = osgViewer::DepthPartitionSettings::FIXED_RANGE;
// 		dps->_zNear = 1;
// 		dps->_zMid = 1900000;
// 		dps->_zFar = 6578000000;
// 		m_viewer->setUpDepthPartition(dps);

		m_configured = true;
	}

	sg::Vec4 OculusWindow::getClearColor() const
	{
		return m_clearColor;
	}
	void OculusWindow::setClearColor(sg::Vec4 const & color)
	{
		m_clearColor = color;
	}

	bool OculusWindow::getEnableDepthPartition() const {
		return m_enableDepthPartition;
	}
	void OculusWindow::setEnableDepthPartition(bool const &isEnable) {
		m_enableDepthPartition = isEnable;
	}

	double OculusWindow::getNearZ() const {
		return m_nearZ;
	}
	void OculusWindow::setNearZ(double const & nearz) {
		m_nearZ = nearz;
	}

	double OculusWindow::getFarZ() const {
		return m_farZ;
	}
	void OculusWindow::setFarZ(double const & farz) {
		m_farZ = farz;
	}

	double OculusWindow::getMidZ() const {
		return m_midZ;
	}
	void OculusWindow::setMidZ(double const & midz) {
		m_midZ = midz;
	}

	double OculusWindow::getPixelsPerDisplayPixel() const {
		return m_pixelsPerDisplayPixel;
	}
	void OculusWindow::setPixelsPerDisplayPixel(double const & ppdp) {
		m_pixelsPerDisplayPixel = ppdp;
	}

	double OculusWindow::getWorldUnitsPerMetre() const {
		return m_worldUnitsPerMetre;
	}
	void OculusWindow::setWorldUnitsPerMetre(double const & wupp) {
		m_worldUnitsPerMetre = wupp;
	}

	int OculusWindow::getSamples() const {
		return m_samples;
	}
	void OculusWindow::setSamples(int const & samples) {
		m_samples = samples;
	}

	/* Public functions */
	void OculusWindow::init()
	{
		// Open the HMD
		OculusDevice::TrackingOrigin origin = OculusDevice::TrackingOrigin::EYE_LEVEL;
		int mirrorTextureWidth = 960;
		m_device = new OculusDevice(m_nearZ, m_farZ, m_pixelsPerDisplayPixel, m_worldUnitsPerMetre, m_samples, origin, mirrorTextureWidth);

		// Exit if we do not have a valid HMD present
		if (!m_device->hmdPresent())
		{
			osg::notify(osg::FATAL) << "Error: No valid HMD present!" << std::endl;
			return;
		}

		// Get the suggested context traits
		osg::ref_ptr<osg::GraphicsContext::Traits> traits = m_device->graphicsContextTraits();
		traits->windowName = "czmosgOculusWindow";

		// Create a graphic context based on our desired traits
		osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());

		if (!gc)
		{
			osg::notify(osg::NOTICE) << "Error, GraphicsWindow has not been created successfully" << std::endl;
			return;
		}

		if (gc.valid())
		{
			gc->setClearColor(sg::convert(m_clearColor));
			gc->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}

		m_viewer = new osgViewer::Viewer();
		// Force single threaded to make sure that no other thread can use the GL context
		m_viewer->setThreadingModel(osgViewer::Viewer::SingleThreaded);
		m_viewer->getCamera()->setGraphicsContext(gc.get());
		m_viewer->getCamera()->setViewport(0, 0, traits->width, traits->height);

		// Disable automatic computation of near and far plane
		m_viewer->getCamera()->setComputeNearFarMode(osg::CullSettings::DO_NOT_COMPUTE_NEAR_FAR);

		// Things to do when viewer is realized
		m_realizeOperation = new OculusRealizeOperation(m_device);
		m_viewer->setRealizeOperation(m_realizeOperation.get());

	#if(OSG_VERSION_GREATER_OR_EQUAL(3, 5, 4))
		// Things to do when viewer is shutting down
		osg::ref_ptr<OculusCleanUpOperation> oculusCleanUpOperation = new OculusCleanUpOperation(m_device);
		m_viewer->setCleanUpOperation(oculusCleanUpOperation.get());
	#endif
	}

	void OculusWindow::setScene(sg::NodePtr const & scene)
	{
		m_oculusScene = new OculusScene(m_viewer, m_device, m_realizeOperation);
		m_oculusScene->addChild(scene);
		m_scene = scene;
		m_viewer->setSceneData(m_oculusScene);
	}

	sg::NodePtr OculusWindow::getScene() const
	{
		return m_scene;
	}

	void OculusWindow::run()
	{
		// Create OculusTouch manipulator
		osg::ref_ptr<OculusTouchManipulator> cameraManipulator = new OculusTouchManipulator(m_device.get());
		m_viewer->setCameraManipulator(cameraManipulator.get());
		cameraManipulator->setHomePosition(osg::Vec3(0, 0, 10), osg::Vec3(0, 0, 0), osg::Vec3(0, 1, 0));

		// Add statistics handler
		m_viewer->addEventHandler(new osgViewer::StatsHandler);

		m_viewer->addEventHandler(new OculusEventHandler(m_device));

		m_viewer->run();
	}
}