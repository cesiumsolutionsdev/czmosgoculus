#ifndef czm_osg_oculus_Controller_hpp
#define czm_osg_oculus_Controller_hpp

#include <czm/osg/oculus/czmOSGOculusAPI.h>

#include <osgGA/CameraManipulator>

namespace osgOculus
{
  /** Light data class which maintains information about a single light and updates
   *  the associated uniforms which will be used in the shader. */
  class CZMOSGOCULUS_API Controller : public osgGA::CameraManipulator
  {
  public:

    Controller();

#if 0
    META_Object( osgOculus, Controller );

    /** set the position of the matrix manipulator using a 4x4 Matrix.*/
    virtual void setByMatrix(const osg::Matrixd& matrix) = 0;

    /** set the position of the matrix manipulator using a 4x4 Matrix.*/
    virtual void setByInverseMatrix(const osg::Matrixd& matrix) = 0;

    /** get the position of the manipulator as 4x4 Matrix.*/
    virtual osg::Matrixd getMatrix() const = 0;

    /** get the position of the manipulator as a inverse matrix of the manipulator, typically used as a model view matrix.*/
    virtual osg::Matrixd getInverseMatrix() const = 0;

    /** update the camera for the current frame, typically called by the viewer classes.
        Default implementation simply set the camera view matrix. */
    virtual void updateCamera(osg::Camera& camera) { camera.setViewMatrix(getInverseMatrix()); }

    /** Get the FusionDistanceMode. Used by SceneView for setting up stereo convergence.*/
    virtual osgUtil::SceneView::FusionDistanceMode getFusionDistanceMode() const { return osgUtil::SceneView::PROPORTIONAL_TO_SCREEN_DISTANCE; }

    /** Get the FusionDistanceValue. Used by SceneView for setting up stereo convergence.*/
    virtual float getFusionDistanceValue() const { return 1.0f; }


    /**
    Attach a node to the manipulator, automatically detaching any previously attached node.
    setNode(NULL) detaches previous nodes.
    May be ignored by manipulators which do not require a reference model.
    */
    virtual void setNode(osg::Node*) {}

    /** Return const node if attached.*/
    virtual const osg::Node* getNode() const { return NULL; }

    /** Return node if attached.*/
    virtual osg::Node* getNode() { return NULL; }

    /** Manually set the home position, and set the automatic compute of home position. */
    virtual void setHomePosition(const osg::Vec3d& eye, const osg::Vec3d& center, const osg::Vec3d& up, bool autoComputeHomePosition=false)
    {
        setAutoComputeHomePosition(autoComputeHomePosition);
        _homeEye = eye;
        _homeCenter = center;
        _homeUp = up;
    }

    /** Get the manually set home position. */
    virtual void getHomePosition(osg::Vec3d& eye, osg::Vec3d& center, osg::Vec3d& up) const
    {
        eye = _homeEye;
        center = _homeCenter;
        up = _homeUp;
    }

    /** Set whether the automatic compute of the home position is enabled.*/
    virtual void setAutoComputeHomePosition(bool flag) { _autoComputeHomePosition = flag; }


    /** Compute the home position.*/
    virtual void computeHomePosition(const osg::Camera *camera = NULL, bool useBoundingBox = false);

    /** finish any active manipulator animations.*/
    virtual void finishAnimation() {}

    /**
    Move the camera to the default position.
    May be ignored by manipulators if home functionality is not appropriate.
    */
    virtual void home(const GUIEventAdapter& ,GUIActionAdapter&) {}

    /**
    Move the camera to the default position.
    This version does not require GUIEventAdapter and GUIActionAdapter so may be
    called from somewhere other than a handle() method in GUIEventHandler.  Application
    must be aware of implications.
    */
    virtual void home(double /*currentTime*/) {}

    /**
    Start/restart the manipulator.
    */
    virtual void init(const GUIEventAdapter& ,GUIActionAdapter&) {}

    /** Handle event. Override the handle(..) method in your event handlers to respond to events. */
    virtual bool handle(osgGA::Event* event, osg::Object* object, osg::NodeVisitor* nv) { return GUIEventHandler::handle(event, object, nv); }

    /** Handle events, return true if handled, false otherwise. */
    virtual bool handle(const GUIEventAdapter& ea,GUIActionAdapter& us);
#endif

  protected:

    virtual ~Controller();

  }; // class Controller

} // namespace osgController

#endif  // czm_osg_oculus_Controller_hpp
