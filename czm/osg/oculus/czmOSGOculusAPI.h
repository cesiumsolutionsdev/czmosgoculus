#ifndef czm_osg_oculus_czmOSGOculusAPI_h
#define czm_osg_oculus_czmOSGOculusAPI_h

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup scenegraph SceneGraph
 * @namespace sg
 * @brief Namespace for all scenegraph oculus classes.
 */

/**
 * @file czmOSGOculusAPI.h
 * @brief Definitions for OSG Oculus Library API
 *
 */

/* Definitions for exporting or importing the OpenIGS OSG Oculus Library API */
#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)
#  if defined czmosgoculus_STATIC
#    define CZMOSGOCULUS_API
#  elif defined czmosgoculus_EXPORTS
#    define CZMOSGOCULUS_API __declspec( dllexport )
#  else
#    define CZMOSGOCULUS_API __declspec( dllimport )
#  endif
#else
#  define CZMOSGOCULUS_API
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif  /* czm_osg_oculus_czmOSGOculusAPI_h */
