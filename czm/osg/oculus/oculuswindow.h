#ifndef _OSG_OCULUSWINDOW_H_
#define _OSG_OCULUSWINDOW_H_

#include <osg/Group>
#include <osgViewer/Viewer>

#include "oculusdevice.h"
#include <czm/osg/oculus/czmOSGOculusAPI.h>
//#include <igs/gui/Element.hpp>

#include <igs/osg/gui/igsOSGGUIAPI.h>

#include <igs/osg/Defs.hpp>
#include <igs/osg/Types.hpp>

#include <igs/math/Euler.hpp>

#include <osg/Node>
#include <osg/NodeCallback>
#include <osg/NodeVisitor>

#include <boost/shared_ptr.hpp>
#include <boost/signal.hpp>

#include <vector>

// Forward declaration
namespace osgViewer
{
	class View;
}

namespace sg {
	typedef osg::ref_ptr<osg::Node>   NodePtr;
}

namespace osgOculus
{
	/** Light data class which maintains information about a single light and updates
	*  the associated uniforms which will be used in the shader. */
	class OculusScene : public osg::Group
	{
	public:
		OculusScene(osgViewer::Viewer* viewer, osg::ref_ptr<OculusDevice> dev, osg::ref_ptr<OculusRealizeOperation> realizeOperation) : osg::Group(),
			m_configured(false),
			m_viewer(viewer),
			m_device(dev),
			m_realizeOperation(realizeOperation)
		{};
		virtual void traverse(osg::NodeVisitor& nv);
	protected:
		~OculusScene() {};
		virtual void configure();

		bool m_configured;

		osg::observer_ptr<osgViewer::Viewer> m_viewer;
		osg::observer_ptr<OculusDevice> m_device;
		osg::observer_ptr<OculusRealizeOperation> m_realizeOperation;
	};

	class CZMOSGOCULUS_API OculusWindow
		//: public igs::gui::Element
	{
	public:
		explicit OculusWindow(/*igs::gui::ContainerBase * parent = 0*/)
			//: igs::gui::Element(igs::gui::Element::ImplPtr(nullptr/*new Impl(parent)*/))
		{
			m_enableDepthPartition = false;
			m_nearZ = 1;
			m_farZ = 249597870;
			m_midZ = 7578;
			m_pixelsPerDisplayPixel = 1.0f;
			m_worldUnitsPerMetre = 1.0;// 1.0f;
			m_samples = 4;
			m_clearColor = { 0, 0, 0, 1.0 };
		};

		void init();

		/** Get the Scene being rendered. */
		sg::NodePtr getScene() const;
		/** Set the Scene being rendered. */
		void setScene(sg::NodePtr const & scene);

		sg::Vec4 getClearColor() const;
		void setClearColor(sg::Vec4 const & color);

		bool getEnableDepthPartition() const;
		void setEnableDepthPartition(bool const &isEnable);

		double getNearZ() const;
		void setNearZ(double const & nearz);

		double getFarZ() const;
		void setFarZ(double const & farz);

		double getMidZ() const;
		void setMidZ(double const & midz);

		double getPixelsPerDisplayPixel() const;
		void setPixelsPerDisplayPixel(double const & ppdp);

		double getWorldUnitsPerMetre() const;
		void setWorldUnitsPerMetre(double const & wupp);

		int getSamples() const;
		void setSamples(int const & samples);

		void run();

	protected:
		~OculusWindow() {};

		osg::ref_ptr<osgViewer::Viewer> m_viewer;
		osg::ref_ptr<OculusDevice> m_device;
		osg::ref_ptr<OculusRealizeOperation> m_realizeOperation;

	private:
		sg::NodePtr m_scene;
		osg::ref_ptr<OculusScene> m_oculusScene;

		sg::Vec4 m_clearColor;
		bool m_enableDepthPartition;
		double m_nearZ;
		double m_farZ;
		double m_midZ;
		double m_pixelsPerDisplayPixel;
		double m_worldUnitsPerMetre;
		int m_samples;
	};

}
#endif /* _OSG_OCULUSWINDOW_H_ */
