#include "Button.hpp"

namespace ovr
{
std::vector<ButtonId> const Button::buttonIds{
  ButtonId::A,
  ButtonId::B,
  ButtonId::RightThumb,
  ButtonId::RightShoulder,
  ButtonId::X,
  ButtonId::Y,
  ButtonId::LeftThumb,
  ButtonId::LeftShoulder,
  ButtonId::Up,
  ButtonId::Down,
  ButtonId::Left,
  ButtonId::Right,
  ButtonId::Enter,
  ButtonId::Back,
  ButtonId::VolumeUp,
  ButtonId::VolumeDown,
  ButtonId::Home
}; // Button::buttonIds

Button::Button(ButtonId buttonId)
: mButtonId(buttonId)
{
} // Button::Button

std::string const & Button::buttonName(ButtonId btnId)
{
  static std::unordered_map<ButtonId, std::string> const sButtonNames
  {
    {ButtonId::A,             "A"},
    {ButtonId::B,             "B"},
    {ButtonId::RightThumb,    "RightThumb"},
    {ButtonId::RightShoulder, "RightShoulder"},
    {ButtonId::X,             "X"},
    {ButtonId::Y,             "Y"},
    {ButtonId::LeftThumb,     "LeftThumb"},
    {ButtonId::LeftShoulder,  "LeftShoulder"},
    {ButtonId::Up,            "Up"},
    {ButtonId::Down,          "Down"},
    {ButtonId::Left,          "Left"},
    {ButtonId::Right,         "Right"},
    {ButtonId::Enter,         "Enter"},
    {ButtonId::Back,          "Back"},
    {ButtonId::VolumeUp,      "VolumeUp"},
    {ButtonId::VolumeDown,    "VolumeDown"},
    {ButtonId::Home,          "Home"},
  };
  static std::string const sUnknown{"Unknown"};

  auto btnit = sButtonNames.find(btnId);
  return btnit != sButtonNames.end() ? btnit->second : sUnknown;
} // Button::buttonName

ButtonConfig & Button::buttonConfig()
{
  static ButtonConfig sButtonConfig;
  return sButtonConfig;
} // Button::buttonConfig;

ButtonEvent Button::update(unsigned int buttons, double timeInSeconds)
{
  bool pressed = mButtonId & buttons;

  // Simple case when there is nothing in the history
  if (mHistory.empty()) {
    // Nothing has happened
    if (!pressed) {
      return ButtonEvent{mButtonId};
    }
    // Initial press
    mHistory.emplace_back(timeInSeconds, pressed);
    return ButtonEvent{mButtonId, ButtonEventType::Pressed};
  }

  double duration = timeInSeconds - mHistory.back().time;

  if (!pressed) {

    switch (mHistory.size()) {

      // The end of the first click, press is in history
      case 1: {
        // For long pressed, no possibility for double-click, so clear history
        if (duration >= buttonConfig().longPressTime) {
          mHistory.clear();
          return ButtonEvent{mButtonId, ButtonEventType::LongPress, duration};
        }

        mHistory.emplace_back(timeInSeconds, false);
        // Don't return a Clicked event yet until the doubleClickPauseTime has expired
        return ButtonEvent{mButtonId};
      }

      // Previous press + release in history
      case 2: {
        // If doubleClickPauseTime is exceeded, we have a normal click,
        // so clear the history
        if (duration >= buttonConfig().doubleClickPauseTime) {
          mHistory.clear();
          return ButtonEvent{mButtonId, ButtonEventType::Clicked};
        }
      }

      // Previous press + release + press in history
      case 3: {
        mHistory.clear();

        // If the second press is a long press, return a LongPress event
        if (duration >= buttonConfig().longPressTime) {
          return ButtonEvent{mButtonId, ButtonEventType::LongPress, duration};
        }
        return ButtonEvent{mButtonId, ButtonEventType::DoubleClicked};
      }

      default:
        // should never happen
        mHistory.clear();
        return ButtonEvent{mButtonId};
    } // history size switch
  } // not pressed
  else {

    // Still pressing, do nothing (yet)
    if (mHistory.back().pressed == true) {
      return ButtonEvent{mButtonId};
    }

    // Start of second click, just store
    mHistory.emplace_back(timeInSeconds, true);
  } // pressed

  return ButtonEvent{mButtonId};
} // Button::update
} // namespace ovr
