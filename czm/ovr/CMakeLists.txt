igs_version( czmOVR Library czmovr CZMOVR_VERSION_SOURCES GLOBAL CZMOVR_API czm/ovr czmOVRAPI.h )

igs_build_target(
    BASENAME      czmovr
    TYPE          Library
    GROUP         OSGOculus
    COMPILE_FLAGS ${IGS_COMPILEFLAGS}
    LIBRARIES
        ${OPENIGS_UTILS_LIBRARY}
        ${SACCADES_LICENSE_LIBRARIES}
        ${OCULUS_SDK_LIBRARIES}
    HEADERS_PUBLIC
        Button.hpp
        ButtonsState.hpp
        czmOVRAPI.h
        Session.hpp
        Utils.hpp
    SOURCES
        Button.cpp
        ButtonsState.cpp
        Session.cpp
        Utils.cpp
	${CZMOVR_VERSION_SOURCES}
    FLAT_SOURCE_TREE
    HEADER_INSTALL_DIR
        include/czm/ovr
)
