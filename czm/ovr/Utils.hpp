#pragma once

#include <czm/ovr/czmOVRAPI.h>

#include <iosfwd>
#include <string>

namespace ovr
{
struct CZMOVR_API ErrorMessage
{
  ErrorMessage(std::string && msg);
  std::string message;
}; // struct ErrorMessage

CZMOVR_API std::ostream & operator<<(std::ostream & os, ErrorMessage const & em);

} // namespace ovr