#pragma once

#include <czm/ovr/czmOVRAPI.h>
#include <czm/ovr/Button.hpp>

#include <unordered_map>

namespace ovr
{
/**
 * Maintains all of the Buttons and is the central place to update 
 */
class CZMOVR_API ButtonsState
{
public:

  ButtonsState();

  /**
   * Updates all internal buttons and 
   */
  ButtonEventVec update(unsigned int buttons, double timeInSeconds);

private:
  using ButtonMap = std::unordered_map<ButtonId, Button>;
  ButtonMap mButtons;
}; // class ButtonsState

} // namespace ovr
