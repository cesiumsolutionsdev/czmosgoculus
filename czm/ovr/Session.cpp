#include "Session.hpp"
#include "Utils.hpp"

#include <OVR_CAPI_GL.h>

#include <iostream>

namespace ovr
{
Session::Session()
: mSession(nullptr)
, mLuid()
{
} // Session::Session

Session::~Session()
{
  if (mSession) {
    ovr_Destroy(mSession);
    ovr_Shutdown();
  }
} // Session::~Session

bool Session::initialize(ovrInitParams const * params)
{
  if (OVR_FAILURE(ovr_Initialize(params))) {
    std::cerr<<ErrorMessage("Initialization failed")<<std::endl;
    return false;
  }
  if (OVR_FAILURE(ovr_Create(&mSession, &mLuid))) {
    std::cerr<<ErrorMessage("Session creation failed")<<std::endl;
    return false;

  }
  return true;
} // Session::initialize
} // namespace ovr
