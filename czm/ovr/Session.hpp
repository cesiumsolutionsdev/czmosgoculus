#pragma once

#include <czm/ovr/czmOVRAPI.h>

#include <OVR_CAPI_GL.h>

namespace ovr
{
class CZMOVR_API Session
{
public:
  Session();

  ~Session();

  bool initialize(ovrInitParams const * params = nullptr);

  ovrSession const & getSession() const { return mSession; }
  ovrGraphicsLuid const & getGraphicsLuid() const { return mLuid; }

  operator ovrSession() const { return mSession; }

private:
  ovrSession      mSession;
  ovrGraphicsLuid mLuid;
}; // class Session

} // namespace ovr