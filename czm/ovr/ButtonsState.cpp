#include "ButtonsState.hpp"

namespace ovr
{
ButtonsState::ButtonsState()
: mButtons()
{
  for (auto btnid : Button::buttonIds) {
    mButtons.emplace(btnid, Button(btnid));
  }
}

ButtonEventVec ButtonsState::update(unsigned int buttons, double timeInSeconds)
{
  ButtonEventVec buttonEvents;
  for(auto btnid : Button::buttonIds) {
    auto buttonEvent = mButtons[btnid].update(buttons, timeInSeconds);
    if (buttonEvent.type != ButtonEventType::None) {
      buttonEvents.push_back(buttonEvent);
    }
  }
  return buttonEvents;
} // ButtonsState::update

} // namespace ovr
