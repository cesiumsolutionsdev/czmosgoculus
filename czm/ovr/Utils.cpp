#include "Utils.hpp"

#include <OVR_CAPI_GL.h>

#include <iostream>

namespace ovr
{
ErrorMessage::ErrorMessage(std::string && msg)
 : message(std::forward<std::string>(msg))
{
}

std::ostream & operator<<(std::ostream & os, ErrorMessage const & em)
{
  ovrErrorInfo errorInfo;
  ovr_GetLastErrorInfo(&errorInfo);
  os<<em.message<<" - OVR Error["<<errorInfo.ErrorString<<']';
  return os;
}

} // namespace ovr
