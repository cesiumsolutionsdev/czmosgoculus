#ifndef czm_ovr_czmOVRAPI_h
#define czm_ovr_czmOVRAPI_h

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup CesiumOculusVR
 * @namespace ovr
 * @brief Namespace for all oculus classes.
 */

/**
 * @file czmOSVRAPI.h
 * @brief Definitions for the Cesium Oculus Library API
 *
 */

/* Definitions for exporting or importing the Cesium Oculus Library API */
#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)
#  if defined czmovr_STATIC
#    define CZMOVR_API
#  elif defined czmovr_EXPORTS
#    define CZMOVR_API __declspec( dllexport )
#  else
#    define CZMOVR_API __declspec( dllimport )
#  endif
#else
#  define CZMOVR_API
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif  /* czm_ovr_czmOVRAPI_h */
