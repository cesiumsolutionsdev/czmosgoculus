#pragma once

#include <czm/ovr/czmOVRAPI.h>

#include <OVR_CAPI_GL.h>

#include <unordered_map>
#include <vector>

namespace ovr
{
/**
 * Enumeration equivalents for the identifiers defined by the OVR C API.
 */
enum class ButtonId : uint32_t
{
  None           = 0,
  A              = ovrButton_A,
  B              = ovrButton_B,
  RightThumb     = ovrButton_RThumb,
  RightShoulder  = ovrButton_RShoulder,
  X              = ovrButton_X,
  Y              = ovrButton_Y,
  LeftThumb      = ovrButton_LThumb,
  LeftShoulder   = ovrButton_LShoulder,
  Up             = ovrButton_Up,
  Down           = ovrButton_Down,
  Left           = ovrButton_Left,
  Right          = ovrButton_Right,
  Enter          = ovrButton_Enter,
  Back           = ovrButton_Back,
  VolumeUp       = ovrButton_VolUp,
  VolumeDown     = ovrButton_VolDown,
  Home           = ovrButton_Home,
  Private        = ovrButton_Private
}; // enum clas ButtonId

template <typename Buttons>
bool operator&(Buttons buttons, ButtonId buttonId)
{
  return buttons & static_cast<Buttons>(buttonId);
}

template <typename Buttons>
inline bool operator&(ButtonId buttonId, Buttons buttons)
{
  return buttons & static_cast<Buttons>(buttonId);
}

/**
 * Defines the characteristic times that determine the nature of the ButtonEvents
 * based on the raw up/down events and the timing between them.
 */
struct ButtonConfig
{
  double clickTime            = 0.1;
  double doubleClickPauseTime = 0.05;
  double longPressTime        = 0.4;
}; // struct ButtonConfig

/**
 * Defines the logical button events based on the raw up/down status changes
 * and the timing between them as defined in the ButtonConfig structure.
 */
enum class ButtonEventType
{
  None,          /**< Button is up and hasn't been pressed recently. */
  Pressed,       /**< Button changed from up to down in the last update. */
  Released,      /**< Button changed from down to up before
                      ButtonConfig.longPressTime time but after
                      ButtonConfig.click/doubleClickTime. */
  Clicked,       /**< Button changed from down to up before
                      ButtonConfig.click/doubleClick time. */
  DoubleClicked, /**< Button was clicked twice within the
                      ButtonConfig.doubleClick time. */
  LongPress      /**< Button has stayed in the down position for longer than
                      the ButtonConfig.longPress time. */
}; // enum class ButtonEventType

struct ButtonEvent
{
  explicit ButtonEvent(ButtonId btnId = ButtonId::None,
                       ButtonEventType bet = ButtonEventType::None,
                       double dur = 0.0)
  : buttonId(btnId)
  , type(bet)
  , duration(dur)
  {}

  ButtonId        buttonId = ButtonId::None;
  ButtonEventType type = ButtonEventType::None;
  double          duration = 0.0; /**< Used only for ButtonEventType::LongPressed. */
}; // struct ButtonEvent

using ButtonEventVec = std::vector<ButtonEvent>;

struct ButtonRawEvent
{
  explicit ButtonRawEvent(double t = 0.0, bool p = false)
  : time(t)
  , pressed(p)
  {}

  double time = 0.0; /**< Event time in seconds. */
  bool   pressed = false;
}; // struct ButtonRawEvent

using ButtonRawEventHistory = std::vector<ButtonRawEvent>;

/**
 * Maintains the state and event calculations of a single Button defined by
 * ButtonId.
 */
class CZMOVR_API Button
{
public:
  Button(ButtonId buttonId = ButtonId::None);

  ButtonId getId() const { return mButtonId; }

  /** The Ids corresponding to the buttons present on the Remote Controller. */
  static unsigned int const remoteButtonIds =
      ovrButton_Up | ovrButton_Down | ovrButton_Left | ovrButton_Right |
      ovrButton_Enter | ovrButton_Back | ovrButton_Home |
      ovrButton_VolUp | ovrButton_VolDown;

  /** The Ids corresponding to the butotns present on the XBox Controller. */
  static unsigned int const xBoxButtonIds =
      ovrButton_A | ovrButton_B | ovrButton_X | ovrButton_Y |
      ovrButton_RShoulder | ovrButton_LShoulder |
      ovrButton_Up | ovrButton_Down | ovrButton_Left | ovrButton_Right |
      ovrButton_Enter | ovrButton_Back;

  /** List of all the ButtonIds (execluding Private) to make iterating through
   *  the numerations simpler. */
  static std::vector<ButtonId> const buttonIds;

  /**
   * Return a human-readable name corresponding to the ButtonId.
   * @param [in] btnId The ButtonId to query the name for.
   * @return The human readable name corresponding to the ButtonId.
   */
  static std::string const & buttonName(ButtonId btnId);

  /**
   * Returns the ButtonConfig containing the timing configurations for
   * determining the logical events based on the raw up/down events.
   */
  static ButtonConfig & buttonConfig();

  /**
   * Calculates the state of all the buttons based on the instantaneous state
   * reported by ovr_GetInputState() in the ovrInputState::TimeinSeconds and
   * ovrInputState::Buttons members.
   * @param [in] buttons The bitmask of the state of all the buttons with the
   *                     bits corresponding to the ButtonId enumeration.
   * @param [in] timeInSections The time in seconds as reported in the
   *                            ovrInputState structure.
   * @see ovrInputState
   */
  ButtonEvent update(unsigned int buttons, double timeInSeconds);

private:
  ButtonId              mButtonId;
  ButtonRawEvent        mPrevEvent;
  ButtonRawEventHistory mHistory;
}; // class Button
} // namespace ovr
