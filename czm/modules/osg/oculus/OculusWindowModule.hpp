#ifndef czm_modules_osg_oculus_OculusModule_hpp
#define czm_modules_osg_oculus_OculusModule_hpp

#include <czm/modules/osg/oculus/czmOSGOculusModulesAPI.h>

#include <czm/osg/oculus/oculuswindow.h>

#include <igs/database/ModuleDevHeader.hpp>

DECLARE_MODULE_CLASS(osgOculus::OculusWindow, sgoculus, OculusWindow, CZMOSGOculusMODULES_API, );
TYPETRAITS(osgOculus::OculusWindow, true, false, false, false);

//DECLARE_ENUMINFO( osgoculus::OculusViewer::OculusType, sgoculus, OculusType, CZMOSGoculusMODULES_API );
//DECLARE_ENUMINFO( osgoculus::OculusViewer::ShadowMode, sgoculus, ShadowMode, CZMOSGoculusMODULES_API );

#endif // czm_modules_osg_oculus_OculusModule_hpp
