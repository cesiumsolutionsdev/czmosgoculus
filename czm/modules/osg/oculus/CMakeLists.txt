igs_version( czmOSGOculusModules Library czmosgoculusmodules CZMOSGOCULUSMODULES_VERSION_SOURCES GLOBAL CZMOSGOCULUSMODULES_API czm/modules/osg/oculus czmOSGOculusModulesAPI.h )

set( CZMOSGOCULUSMODULES_SOURCES
    OculusWindowModule.cpp
    precompiled.cpp
)
set( PRECOMPILE_SOURCES
    ${CZMOSGOCULUSMODULES_SOURCES}
)
setup_precompiled_header( precompiled.h precompiled.cpp PRECOMPILE_SOURCES )

set( EXAMPLES
    examples/simpleoculus.igsml
)

set( EARTH_DEMO 
    examples/earth/oculus_earth.igsml
    examples/earth/earth-scene.igsml
    examples/earth/earth-rotation.igsml
)

source_group(
    \\examples FILES
    ${EXAMPLES} 
)

source_group(
	\\demo FILES
	${EARTH_DEMO}
)

install(
    FILES ${EXAMPLES}
    DESTINATION share/saccades/examples/osg
)
install(
    FILES ${EARTH_DEMO}
    DESTINATION share/saccades/examples/osg/earth
)

igs_build_target(
    BASENAME      czmosgoculusmodules
    TYPE          Library
    GROUP         OSGOculus
    COMPILE_FLAGS ${IGS_COMPILEFLAGS}
    LIBRARIES
        czmosgoculus
        optimized igsosgmodules debug igsosgmodules${CMAKE_DEBUG_POSTFIX}
        optimized igsosg        debug igsosg${CMAKE_DEBUG_POSTFIX}
        ${OPENIGS_LIBRARIES}
        ${SACCADES_LICENSE_LIBRARIES}
        ${OPENSCENEGRAPH_LIBRARIES}
        ${OPENGL_LIBRARIES}
    HEADERS_PUBLIC
        czmOSGOculusModulesAPI.h
        OculusWindowModule.hpp
    HEADERS_PRIVATE
        precompiled.h
    SOURCES
        ${CZMOSGOCULUSMODULES_SOURCES}
        ${CZMOSGOCULUSMODULES_VERSION_SOURCES}
        ${PRECOMPILE_SOURCES}
        ${EXAMPLES}
    FLAT_SOURCE_TREE
    HEADER_INSTALL_DIR
        include/igs/modules/osg/oculus
)
