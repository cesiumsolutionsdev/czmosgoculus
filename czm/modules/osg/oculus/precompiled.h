#if defined USE_PRECOMPILED_HEADERS

#if defined WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  define _WINSOCKAPI_
#endif

#include <igs/application/ApplicationHeaders.hpp>
#include <igs/conversion/ConversionHeaders.hpp>
#include <igs/database/DatabaseHeaders.hpp>
#include <igs/dataio/DataIOHeaders.hpp>
#include <igs/link/LinkHeaders.hpp>
#include <igs/reflection/ReflectionHeaders.hpp>
#include <igs/util/UtilityHeaders.hpp>

#include <boost/shared_ptr.hpp>
#include <boost/signal.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/cstdint.hpp>

#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <string>
#include <memory>

#endif /* defined USE_PRECOMPILED_HEADERS */
