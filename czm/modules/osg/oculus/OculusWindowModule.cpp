#include "precompiled.h"
#include "OculusWindowModule.hpp"

#include <igs/modules/osg/OSGDefs.hpp>
#include <igs/database/ModuleDevSource.hpp>
#include <igs/modules/osg/scenegraph/NodeModule.hpp>

// ----------------------------------------------------------------------------
// OculusWindow Module definitions
// ----------------------------------------------------------------------------

namespace igs {

template <>
ClassInfo const &
classInfo<osgOculus::OculusWindow>()
{
  static ClassInfo info = ClassInfo().init<osgOculus::OculusWindow>()

	  .addConstructor(constructor< osgOculus::OculusWindow, osgOculus::OculusWindow* >())
	  /*.addConstructor(
		  constructor<osgOculus::OculusWindow, osgOculus::OculusWindow *, igs::gui::ContainerBase *>(
			  Parameter("parent", (igs::gui::ContainerBase *)0, Parameter::In)
			  )
		  )*/

	  .addProperty(
		  Property("ClearColor", Property::Persistent,
			  Method("getClearColor", &osgOculus::OculusWindow::getClearColor,
				  Return("clearColor", igs::math::initVec<sg::Vec4>(1.0), Return::Out)
				  ),
			  Method("setClearColor", &osgOculus::OculusWindow::setClearColor,
				  Parameter("clearColor", igs::math::initVec<sg::Vec4>(1.0), Parameter::In)
				  ),
			  Signal()
			  )
		  )

	  .addProperty(
		  Property("EnableDepthPartition", Property::RunTime,
			  Method("getEnableDepthPartition", &osgOculus::OculusWindow::getEnableDepthPartition,
				  Return("enableDepthPartition", bool(), Return::Out)
				  ),
			  Method("setEnableDepthPartition", &osgOculus::OculusWindow::setEnableDepthPartition,
				  Parameter("enableDepthPartition", bool(), Parameter::In)
				  ),
			  Signal()
			  )
		  )

	  .addProperty(
		  Property("NearZ", Property::RunTime,
			  Method("getNearZ", &osgOculus::OculusWindow::getNearZ,
				  Return("nearZ", double(), Return::Out)
				  ),
			  Method("setNearZ", &osgOculus::OculusWindow::setNearZ,
				  Parameter("nearZ", double(), Parameter::In)
				  ),
			  Signal()
			  )
		  )

	  .addProperty(
		  Property("Initialize", Property::RunTime,
			  Operation(),
			  Method("initialize", &osgOculus::OculusWindow::init),
			  Signal()
			  )
		  )

	  .addProperty(
		  Property("Scene", Property::Persistent,
			  Method("getScene", &osgOculus::OculusWindow::getScene,
				  Return("scene", sg::NodePtr(), Return::Out)
				  ),
			  Method("setScene", &osgOculus::OculusWindow::setScene,
				  Parameter("scene", sg::NodePtr(), Parameter::In)
				  ),
			  Signal()
			  )
		  )

	  .addProperty(
		  Property("Run", Property::RunTime,
			  Operation(),
			  Method("run", &osgOculus::OculusWindow::run),
			  Signal()
			  )
		  )
  ;
  return info;
} // classInfo<osgLighting::Light>

} // namespace igs

DEFINE_MODULE_CLASS(osgOculus::OculusWindow, sgoculus, OculusWindow);

