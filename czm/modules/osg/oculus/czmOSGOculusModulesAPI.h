#ifndef czm_modules_osg_oculus_czmOSGOculusModulesAPI_h
#define czm_modules_osg_oculus_czmOSGOculusModulesAPI_h

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup scenegraph SceneGraph
 * @namespace sg
 * @brief Namespace for all scenegraph oculus-related modules.
 */

/**
 * @file czmOSGOculusModulesAPI.h
 * @brief Definitions for OSG Oculus Module API
 *
 */

/* Definitions for exporting or importing the OpenIGS OSG Oculus Modules API */
#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)
#  if defined czmosgoculusmodules_STATIC
#    define CZMOSGOculusMODULES_API
#  elif defined czmosgoculusmodules_EXPORTS
#    define CZMOSGOculusMODULES_API __declspec( dllexport )
#  else
#    define CZMOSGOculusMODULES_API __declspec( dllimport )
#  endif
#else
#  define CZMOSGOculusMODULES_API
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif  /* czm_modules_osg_oculus_czmOSGOculusModulesAPI_h */
