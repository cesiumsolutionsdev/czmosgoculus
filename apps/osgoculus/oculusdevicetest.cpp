#include <czm/ovr/Button.hpp>
#include <czm/ovr/ButtonsState.hpp>
#include <czm/ovr/Session.hpp>
#include <czm/ovr/Utils.hpp>

#include <OVR_CAPI_GL.h>

#include <chrono>
#include <iostream>

#include <cmath>
#if !defined M_PI
#define M_PI 3.14159265358
#endif
#define Rad2Deg( _rad ) ((180*_rad)/M_PI)
#define Deg2Rad( _deg ) ((_rad*M_PI)/180.0)


static ovr::ButtonsState sButtonStates;

std::ostream & operator<<(std::ostream & os, ovrHmdDesc const & hmdDesc)
{
  os<<"Product:         "<<hmdDesc.ProductName<<'\n'
    <<"Manufacturer:    "<<hmdDesc.Manufacturer<<'\n'
    <<"VendorId:        "<<hmdDesc.VendorId<<'\n'
    <<"ProductId:       "<<hmdDesc.ProductId<<'\n'
    <<"SerialNumber:    "<<hmdDesc.SerialNumber<<'\n'
    <<"FirmwareVersion: "<<hmdDesc.FirmwareMajor<<"."<<hmdDesc.FirmwareMinor<<'\n';
  return os;
}

std::ostream & operator<<(std::ostream & os, ovrInputState const & state)    
{
  std::cout<<std::boolalpha;

  if (state.Buttons & ovr::Button::remoteButtonIds) {
    os<<"Time: "<<state.TimeInSeconds<<'\n';

    for (auto btnid : ovr::Button::buttonIds) {
      if (state.Buttons & btnid) {
        os<<"    "<<ovr::Button::buttonName(btnid)<<'\n';
      }
    }
  }

  return os;
}

int main( int argc, char ** argv )
{
  ovr::Session session;

  if (!session.initialize(nullptr)) {
    std::cerr<<ovr::ErrorMessage("Error initializing Oculus")<<std::endl;
    return 1;
  }

  ovrSessionStatus status;
  if (OVR_FAILURE(ovr_GetSessionStatus(session, &status))) {
    std::cerr<<ovr::ErrorMessage("Error getting session status")<<std::endl;
    return 3;
  }

  if (!status.HmdPresent) {
    std::cerr<<ovr::ErrorMessage("HMD not present")<<std::endl;
    return 4;
  }

  ovrHmdDesc hmdDesc = ovr_GetHmdDesc(session);
  std::cout<<hmdDesc<<std::endl;

  unsigned int numTrackers = ovr_GetTrackerCount( session );
  for ( unsigned int tidx = 0; tidx < numTrackers; ++tidx ) {
    ovrTrackerDesc trackerDesc = ovr_GetTrackerDesc( session, tidx );
    std::cout<<"Tracker: "<<tidx<<'\n'
             <<"  HFOV:  "<<Rad2Deg(trackerDesc.FrustumHFovInRadians * 180/M_PI)<<'\n'
             <<"  VFOV:  "<<Rad2Deg(trackerDesc.FrustumVFovInRadians * 180/M_PI)<<'\n'
             <<"  ZNear: "<<trackerDesc.FrustumNearZInMeters<<'n'
             <<"  ZFar:  "<<trackerDesc.FrustumFarZInMeters<<std::endl;
  }

  long long frameIndex = 0;
  ovrInputState controllerState;
  ovrLayerEyeFov layerEyeFov;
  ovrViewScaleDesc viewScale;

  while (1) {
    if (OVR_FAILURE(ovr_WaitToBeginFrame(session, frameIndex))) {
      std::cerr<<ovr::ErrorMessage("Error during Wait to Begin Frame")<<std::endl;
      return 5;
    }
    if (OVR_FAILURE(ovr_BeginFrame(session, frameIndex))) {
      std::cerr<<ovr::ErrorMessage("Error during Begin Frame")<<std::endl;
      return 6;
    }

    if (OVR_FAILURE(ovr_GetInputState(session, ovrControllerType_Remote, &controllerState))) {
      std::cerr<<ovr::ErrorMessage("Error getting Input State")<<std::endl;
      return 7;
    }
    if (controllerState.ControllerType == ovrControllerType_Remote) {
      std::cout<<controllerState<<'\n';
    }

    ovrLayerHeader * layers = &layerEyeFov.Header;
    if (OVR_FAILURE(ovr_EndFrame(session, frameIndex, &viewScale, &layers, 1))) {
      std::cerr<<ovr::ErrorMessage("Error during End Frame")<<std::endl;
      return 8;
    }
    ++frameIndex;
  }

}

