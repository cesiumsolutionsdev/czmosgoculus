#include <osgDB/ReadFile>
#include <osgGA/TrackballManipulator>
#include <osgViewer/Viewer>
#include <osgUtil/GLObjectsVisitor>

#include <czm/osg/oculus/oculuswindow.h>

int main(int argc, char** argv)
{
	// use an ArgumentParser object to manage the program arguments.
	osg::ArgumentParser arguments(&argc, argv);
	// read the scene from the list of file specified command line arguments.
	osg::ref_ptr<osg::Node> loadedModel = osgDB::readNodeFiles(arguments);

	// if not loaded assume no arguments passed in, try use default cow model instead.
	if (!loadedModel) { loadedModel = osgDB::readNodeFile("cow.osgt"); }

	// Still no loaded model, then exit
	if (!loadedModel)
	{
		osg::notify(osg::ALWAYS) << "No model could be loaded and didn't find cow.osgt, terminating.." << std::endl;
		return 0;
	}

	osgOculus::OculusWindow* oculusWindow = new osgOculus::OculusWindow();
	oculusWindow->init();
	oculusWindow->setScene(loadedModel);
	oculusWindow->getScene();
	oculusWindow->run();

	return 0;
}
